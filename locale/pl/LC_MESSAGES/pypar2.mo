��    ?        Y         p     q     �     �     �     �  *   �          )  	   5     ?     L     ]     d     j     q  
   �     �  I   �  1   �       (   ,     U  -   k     �     �  '   �     �     �          (  .   /     ^     u  -   �     �     �     �     �     �     �  
   	  
   	     	     /	  2   >	     q	  $   x	     �	     �	     �	  0   �	  
   
     
     %
  &   ;
     b
  +   q
  )   �
  "   �
     �
     �
     �
  {  	     �     �     �     �     �  '         (     C  	   Q     [     k          �  	   �  ,   �     �  
   �  E   �  1   $  #   V  2   z     �  9   �            '   $  /   L     |  )   �     �  0   �     �        7   ;     s     �     �     �     �      �     �     �     �     	  >        W  :   ^     �     �     �  2   �     �  )        <  :   W     �  +   �  6   �  "        *     0  #   7        <   =          +            $             ?           	   "   
       8       3   %       &      5                  ;       ,         :           6   4                    (          '              !   -   2       7   9   #         .   *                                             0      /         1      )   >             has an unsupported type  is an unknown preference  is an unsupported type <b>Action to perform:</b> <b>Par2 file:</b> A graphical frontend for the par2 utility. Add directory content Add file(s) Beginning Block count: Block size (KB): Blocks Check Create Default advanced settings Developer: Dynamic Dynamic computation of the number of blocks the files will be sliced into Dynamic computation of the number of parity files Files to protect: First recovery block in the parity files First recovery block: Force all parity files to be of the same size Go Level of redundancy (%): Level of redundancy of the parity files Limit size of parity files Maximum memory usage Maximum memory usage (MB): Memory Number of blocks the files will be sliced into Number of parity files Number of parity files: Number of recovery blocks in the parity files Par2 file to create Parity files Pause PyPar2 Quit Recovery block count: Redundancy Remove all Remove selected file Repair file(s) Restore the default value of all advanced settings Resume Right-click to add/remove some files Select A Par2 File Select directory Select file(s) Size of the blocks the files will be sliced into Thanks to: Uniform parity file size Use advanced settings Verify and then repair files if needed Verify file(s) Verify files, but do not try to repair them You must first add some files to protect! You must first select a par2 file! _File _Help translator-credits Project-Id-Version: pypar2 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-27 21:33+0100
PO-Revision-Date: 2006-12-07 12:00+0100
Last-Translator: Piotr Ożarowski <ozarow@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  ma niewspierany typ  nie jest obsługiwaną opcją  jest niewspieranym typem <b>Akcja do wykonania:</b> <b>Plik par2:</b> Graficzna nakładka na narzędzie par2. Dodaj zawartość katalogu Dodaj plik(i) Początek Liczna bloków: Rozmiar bloku (KB): Bloki Kontrola Tworzenie Domyślne wartości dla opcji zaawansowanych Rozwijający: Dynamiczne Dynamiczne obliczenie liczby bloków na jakie podzielone będą pliki Dynamiczne obliczenie liczby plików parzystości Pliki, które mają być chronione: Pierwszy blok naprawiający w plikach parzystości Pierwszy blok naprawiający: Wymuś identyczny rozmiar wszystkich plików parzystości Uruchom Poziom redundancji (%): Poziom redundancji plików parzystości Ograniczenie wielkości plików naprawiających Maksymalna zużycie pamięci Maksymalna ilość użytej pamięci (MB): Pamięć Ilość bloków na jakie będą podzielone pliki Ilość plików parzystości Ilość plików naprawiających: Ilość bloków naprawiąjących w plikach parzystości Plik par2 do stworzenia Pliki parzystości Pauza PyPar2 Wyjście Ilość bloków naprawiających: Redundancja Wyczyść listę Usuń zaznaczone pliki Napraw plik(i) Przywróć domyślne wartości wszystkich opcji zaawansowanych Wznów Kliknij prawym przyciskiem myszy aby dodać/usunąć pliki Wybierz plik par2 Wybierz katalog Wybierz plik(i) Rozmiar bloków na jakie zostaną podzielone pliki Podziękowania dla: Jednakowy rozmiar plików naprawiających Użyj opcji zaawansowanych Weryfikuj pliki i napraw je, jeżeli zajdzie taka potrzeba Weryfikuj plik(i) Weryfikuj pliki, ale nie próbuj naprawiać Musisz najpierw wybrać pliki, które chcesz chronić! Musisz najpierw wybrać plik par2! _Plik P_omoc Piotr Ożarowski <ozarow@gmail.com> 