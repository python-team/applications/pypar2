��    ?        Y         p     q     �     �     �     �  *   �          )  	   5     ?     L     ]     d     j     q  
   �     �  I   �  1   �          +  (   =     f  -   |     �     �  '   �     �     	          9  .   @     o     �  -   �     �     �     �     �     �  
   	  
   	     $	     9	  2   H	     {	  $   �	     �	     �	     �	  0   �	  
   
     
     /
  &   E
     l
  +   {
  )   �
  "   �
     �
     �
        �    2   �  !   �  ,        I     [  Q   j  %   �     �        "        0  
   N     Y     l  ;   {     �     �  �   �  Z   �     �       U   (  3   ~  `   �       ,      4   M  G   �  I   �  6        K  |   X  6   �  -     Y   :  )   �     �  
   �  
   �  5   �          2  *   H  %   s  J   �     �  m   �     i  #   �     �  @   �       @   "     c  f   v  !   �  W   �  O   W  >   �  	   �     �  +   �        <   =          +            %             ?           	   #   
       8       3   &       '      5                  ;      ,         :   !       6   4                    (                         "   -   2       7   9   $         .   *                                              0      /         1      )   >             has an unsupported type  is an unknown preference  is an unsupported type <b>Action to perform:</b> <b>Par2 file:</b> A graphical frontend for the par2 utility. Add directory content Add file(s) Beginning Block count: Block size (KB): Blocks Check Create Default advanced settings Developer: Dynamic Dynamic computation of the number of blocks the files will be sliced into Dynamic computation of the number of parity files Files to protect Files to protect: First recovery block in the parity files First recovery block: Force all parity files to be of the same size Go Level of redundancy (%): Level of redundancy of the parity files Limit size of parity files Maximum memory usage Maximum memory usage (MB): Memory Number of blocks the files will be sliced into Number of parity files Number of parity files: Number of recovery blocks in the parity files Par2 file to create Parity files Pause Quit Recovery block count: Redundancy Remove all Remove selected file Repair file(s) Restore the default value of all advanced settings Resume Right-click to add/remove some files Select A Par2 File Select directory Select file(s) Size of the blocks the files will be sliced into Thanks to: Uniform parity file size Use advanced settings Verify and then repair files if needed Verify file(s) Verify files, but do not try to repair them You must first add some files to protect! You must first select a par2 file! _File _Help translator-credits Project-Id-Version: PyPar 2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-11 11:25+0200
PO-Revision-Date: 2007-04-11 11:25+0200
Last-Translator: Solomin Evgeny <opensystems.nnov@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 имеет неподдерживаемый тип неизвестная опция  неподдерживаемого типа Действие: Par2 файл: Графическая утилита для работы с файлами par2. Добавить директорию Добавить файл(ы) Начало Количество блоков: Размер блока (KB): Блоки Проверить Создать Параметры настроек по умолчанию Разработка: Динамически Динамическое вычисление числа блоков в файлах, которые получатся после разбиения, на выходе Динамическое вычисление числа паритетных файлов Файлы для защиты Файлы для защиты: Первый восстановочный блок в паритетном файле Первый восстановочный блок: Все паритетные файлы должны иметь одинаковый размер Начать Уровень избыточности (%): Уровень избыточности файлов Максимальный размер паритетного файла Максимальный объем используемой памяти Максимальный объем памяти (MB): Память Число блоков в файлах, которые получаться после разбиения на выходе Количество паритетных файлов Число паритетных файлов: Число восстановочных блоков в паритетных файлах Par2 файл, что бы создать Par файлы Пауза Выход Число восстановочных блоков: Избыточность Удалить все Удалить выбранный файл Восстановить файл(ы) Восстановить все настройки по умолчанию Возобновить Щелкните правой кнопкой мышы, что бы добавить/удалить файлы Выбрать Par2 Файл Выбрать директорию Выбрать файл(ы) Размер блоков файлов для разбиения Благодарности Одинаковый размер файлов на выходе Настройки Проверить и затем при необходимости восстановить файлы Проверить файлы(ы) Проверить файлы, но не пытаться восстановить их Сначала Вы должны добавить файлы к проекту! Сначала Вы должны выбрать par2 файл! _Файл _Помощь Перевод <opensystems.nnov@gmail.com> 