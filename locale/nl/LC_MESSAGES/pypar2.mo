��    @        Y         �     �     �     �     �     �  *   �     #     9  	   E     O     \     m     t     z     �  
   �     �  I   �  1   �     *     ;  (   M     v  -   �     �     �  '   �     �          .     I  .   P          �  -   �     �     �     �     	     
	     	  
   %	  
   0	     ;	     P	  2   _	     �	  $   �	     �	     �	     �	  0   �	  
   "
     -
     F
  &   \
     �
  +   �
  )   �
  "   �
                 �  *      �     �     �          0  %   E     k          �     �     �     �     �     �  #   �     �  	     X     3   o     �     �     �     �  $        :      @  b   a  #   �     �             8   '     `     v  *   �     �     �     �     �  	   �     �                (     I  >   _  	   �  7   �     �     �     
  C   !     e     s  #   �  :   �     �  *   	  1   4      f     �     �  S   �        =   >          ,            %             @           	   #   
       9       4   &       '      6                  <      -         ;   !       7   5                    )          (              "   .   3       8   :   $         /   +                                              1      0         2      *   ?             has an unsupported type  is an unknown preference  is an unsupported type <b>Action to perform:</b> <b>Par2 file:</b> A graphical frontend for the par2 utility. Add directory content Add file(s) Beginning Block count: Block size (KB): Blocks Check Create Default advanced settings Developer: Dynamic Dynamic computation of the number of blocks the files will be sliced into Dynamic computation of the number of parity files Files to protect Files to protect: First recovery block in the parity files First recovery block: Force all parity files to be of the same size Go Level of redundancy (%): Level of redundancy of the parity files Limit size of parity files Maximum memory usage Maximum memory usage (MB): Memory Number of blocks the files will be sliced into Number of parity files Number of parity files: Number of recovery blocks in the parity files Par2 file to create Parity files Pause PyPar2 Quit Recovery block count: Redundancy Remove all Remove selected file Repair file(s) Restore the default value of all advanced settings Resume Right-click to add/remove some files Select A Par2 File Select directory Select file(s) Size of the blocks the files will be sliced into Thanks to: Uniform parity file size Use advanced settings Verify and then repair files if needed Verify file(s) Verify files, but do not try to repair them You must first add some files to protect! You must first select a par2 file! _File _Help translator-credits Project-Id-Version: nl.po_[UcRRnb]
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-27 21:33+0100
PO-Revision-Date: 2007-02-22 22:24+0100
Last-Translator: Rinse de Vries <rinsedevries@home.nl>
Language-Team:  <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Dutch
X-Poedit-Country: Netherlands, The
X-Generator: KBabel 1.11.4
  heeft een niet-ondersteund type  is een onbekende voorkeur  is een niet-ondersteund type <b>Uit te voeren actie:</b> <b>PAR2-bestand:</b> Een grafische schil voor par2cmdline. Mapinhoud toevoegen Bestand(en) toevoegen Begin Aantal blokken: Blokgrootte (KB): Blokken Controleren Aanmaken Standaard geavanceerde instellingen Ontwikkelaar: Dynamisch Dynamische berekening van het aantal blokken waarin de bestanden zullen worden opgedeeld Dynamische berekening van het aantal PAR2-bestanden Bestanden om te beschermen Bestanden om te beschermen: Nummering van de PAR2-bestanden Nummer eerste herstelblok: Alle PAR2-bestanden even groot maken Start Hoeveelheid herstelgegevens (%): Hoeveelheid herstelgegevens van de PAR2-bestanden uigedrukt in percentage van het oringele bestand Grootte van PAR2-bestanden beperken Maximum geheugengebruik Maximum geheugengebruik (MB): Geheugen Aantal blokken waarin de bestanden gedeeld zullen worden Aantal PAR2-bestanden Aantal PAR2-bestanden: Aantal herstelblokken in de PAR2-bestanden Aan te maken PAR2-bestand PAR2-bestanden Pauze PyPar2 Afsluiten Aantal herstelblokken: Volume Alles verwijderen Geselecteerd bestand verwijderen Bestand(en) repareren Herstel de standaardwaarden van alle geavanceerde instellingen Hervatten Rechtsklik om bestanden toe te voegen of te verwijderen Selecteer een PAR2-bestand Map selecteren Bestand(en) selecteren Grootte van de blokken waar de bestanden in opgedeeld zullen worden Met dank aan: Maak PAR2-bestanden even groot Geavanceerde instellingen gebruiken Bestanden verifiëren en vervolgens repareren indien nodig Bestand(en) verifiëren Bestanden verifiëren, maar niet repareren Voeg eerst enkele bestanden om te beschermen toe. Selecteer eerst een PAR2-bestand _Bestand _Help Sander Smalbrugge <sandersmalbrugge@gmail.com>
Rinse de Vries <rinsedevries@kde.nl> 