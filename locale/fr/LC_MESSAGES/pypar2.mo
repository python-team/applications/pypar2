��    ?        Y         p     q     �     �     �     �  *   �          )  	   5     ?     L     ]     d     j     q  
   �     �  I   �  1   �       (   ,     U  -   k     �     �  '   �     �     �          (  .   /     ^     u  -   �     �     �     �     �     �     �  
   	  
   	     	     /	  2   >	     q	  $   x	     �	     �	     �	  0   �	  
   
     
     %
  &   ;
     b
  +   q
  )   �
  "   �
     �
     �
     �
  k  	     u     �     �     �     �  /   �  #   %     I     ^     e     u     �  	   �     �     �     �  	   �  K   �  8   #     \      t      �  F   �     �        1        E  #   d     �     �  7   �  $   �     
  3        S     j     �     �     �  #   �  
   �     �      �     �  5     	   C  ,   M     z     �     �  6   �  
             ,  1   K     }  3   �  4   �  1   �     /     8  (   >        <   =          +            $             ?           	   "   
       8       3   %       &      5                  ;       ,         :           6   4                    (          '              !   -   2       7   9   #         .   *                                             0      /         1      )   >             has an unsupported type  is an unknown preference  is an unsupported type <b>Action to perform:</b> <b>Par2 file:</b> A graphical frontend for the par2 utility. Add directory content Add file(s) Beginning Block count: Block size (KB): Blocks Check Create Default advanced settings Developer: Dynamic Dynamic computation of the number of blocks the files will be sliced into Dynamic computation of the number of parity files Files to protect: First recovery block in the parity files First recovery block: Force all parity files to be of the same size Go Level of redundancy (%): Level of redundancy of the parity files Limit size of parity files Maximum memory usage Maximum memory usage (MB): Memory Number of blocks the files will be sliced into Number of parity files Number of parity files: Number of recovery blocks in the parity files Par2 file to create Parity files Pause PyPar2 Quit Recovery block count: Redundancy Remove all Remove selected file Repair file(s) Restore the default value of all advanced settings Resume Right-click to add/remove some files Select A Par2 File Select directory Select file(s) Size of the blocks the files will be sliced into Thanks to: Uniform parity file size Use advanced settings Verify and then repair files if needed Verify file(s) Verify files, but do not try to repair them You must first add some files to protect! You must first select a par2 file! _File _Help translator-credits Project-Id-Version: PyPar 2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-27 21:33+0100
PO-Revision-Date: 2006-12-07 19:03+0100
Last-Translator: Francois Ingelrest <Athropos@gmail.com>
Language-Team: French <traduc@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
  possède un type non supporté  est une préférence inconnue  est un type non supporté <b>Action :</b> <b>Fichier par2 :</b> Une interface graphique pour le programme par2. Ajouter le contenu d'un répertoire Ajouter des fichiers Début Nombre de blocs Taille de bloc (Ko) Blocs Vérifier Créer Options avancées par défaut Développeur : Dynamique Calcul dynamique du nombre de blocs à utiliser pour découper les fichiers Calcul dynamique du nombre de fichiers de récupération Fichiers à protéger : Premier bloc de récuépération Premier bloc de récupération : Forcer tous les fichiers de récupération à être de la même taille Ok Taux de redondance Taux de redondance des fichiers de récupération Limiter la taille des fichiers Utilisation maximale de la mémoire Utilisation maximale (Mo) : Mémoire Nombre de blocs à utiliser pour découper les fichiers Nombre de fichiers de récupération Nombre de fichiers : Nombre de blocs dans les fichiers de récupération Fichier par2 à créer Fichiers de récupération Pause PyPar2 Quitter Nombre de blocs de récupération : Redondance Tout retirer Retirer le fichier sélectionné Réparer les fichiers Restaurer la valeur par défaut des options avancées Reprendre Clic droit pour ajouter/retirer des fichiers Choisissez un fichier par2 Sélectionnez un répertoire Sélectionnez des fichiers Taille de bloc à utiliser pour découper les fichiers Merci à : Utiliser une taille uniforme Utiliser les options avancées Vérifier et réparer les fichiers si nécessaire Vérifier les fichiers Vérifier les fichiers sans essayer de les réparer Vous devez d'abord ajouter des fichier à protéger! Vous devez d'abord sélectionner un fichier par2! _Fichier _Aide Ingelrest François <Athropos@gmail.com> 