��    =        S   �      8     9     R     l     �     �  *   �     �     �  	   �               %     ,     2     9  
   S     ^  I   f  1   �     �  (   �       -   3     a     d  '   }     �     �     �     �  .   �     &     =  -   U     �     �     �     �     �  
   �  
   �     �     �  2    	  $   3	     X	     k	     |	  0   �	  
   �	     �	     �	  &   �	     
  +   ,
  )   X
  "   �
     �
     �
     �
  g  �
  #   ,     P     p     �     �  %   �     �                     +     ?     E     M      V     w  	   �  >   �  6   �               :  2   Z     �     �     �  $   �     �          #  7   ,     d          �     �     �     �     �  !   �  	             '     E  +   Y  8   �     �     �     �  <   �     6  $   ?     d  !   }     �  (   �  =   �          :     A     H                      4               (                    .                        8       =   $      0          /   )         ,   ;       
   2              6   *       !   %              :   +   -          1             #      '                 &   7            9   	   3          <   5   "              has an unsupported type  is an unknown preference  is an unsupported type <b>Action to perform:</b> <b>Par2 file:</b> A graphical frontend for the par2 utility. Add directory content Add file(s) Beginning Block count: Block size (KB): Blocks Check Create Default advanced settings Developer: Dynamic Dynamic computation of the number of blocks the files will be sliced into Dynamic computation of the number of parity files Files to protect: First recovery block in the parity files First recovery block: Force all parity files to be of the same size Go Level of redundancy (%): Level of redundancy of the parity files Limit size of parity files Maximum memory usage Maximum memory usage (MB): Memory Number of blocks the files will be sliced into Number of parity files Number of parity files: Number of recovery blocks in the parity files Par2 file to create Parity files PyPar2 Quit Recovery block count: Redundancy Remove all Remove selected file Repair file(s) Restore the default value of all advanced settings Right-click to add/remove some files Select A Par2 File Select directory Select file(s) Size of the blocks the files will be sliced into Thanks to: Uniform parity file size Use advanced settings Verify and then repair files if needed Verify file(s) Verify files, but do not try to repair them You must first add some files to protect! You must first select a par2 file! _File _Help translator-credits Project-Id-Version: PyPar2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-27 21:33+0100
PO-Revision-Date: 2006-12-07 20:55+0100
Last-Translator: Volker Eckert <veckert@gmx.net>
Language-Team:  <Athropos@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
  hat einen nicht unterstützten Typ ist eine unbekannte Einstellung  Nicht unterstützter Typ <b>Auszuführende Aktion:</b> <b>Par2 Datei:</b> Eine grafische Oberfläche für par2. Verzeichnis Inhalt hinzufügen Datei(en) hinzufügen Begin Block Zähler: Block Größe (KB): Block Prüfen Erzeugen Vorgabe erweiterte Einstellungen Entwickler: Dynamisch Dynamische Berechnung der Blockanzahl der zu teilenden Dateien Dynamische Berechnung der Anzahl von Paritäts-Dateien Zu schützende Dateien: Erster Wiederherstellungsblock Erster Wiederherstellungsblock: Alle Paritäts-Dateien auf gleiche Größe bringen Start Grenze der Redundanz (%): Grenze der Redundanz (%) Größengrenze der Reparatur-Dateien Maximale Speichernutzung Maximale Speichernutzung (MB): Speicher Anzahl der Blöcke in welche die Dateien geteilt werden Zahl der Reparatur-Dateien Zahl der Reparatur-Dateien: Zahl der Reparatur-Dateien Par2 Datei erzeugen Dateien vergleichen PyPar2 Beenden Wiederherstellungs Block Zähler: Redundanz Entferne alle Entferne ausgewählte Dateien Repariere Datei(en) Rücksetzen aller erweiterten Einstellungen Rechts-Klick zum Hinzufügen/Entfernen einzelner Dateien Wähle eine Par2 Datei Wähle Verzeichnis Wähle Datei(en) Größe der Blöcke der in welche die Dateien geteilt werden Dank an: Einheitliche Reparatur-Datei Größe Erweiterte Einstellungen Vergleiche, wenn nötig repariere Dateien vergleichen Vergleiche Dateien, aber repariere nicht Um Dateien zu schützen müssen sie erst hinzugefügt werden! Zuerst eine par2 Datei wählen! _Datei _Hilfe Volker Eckert <veckert@gmx.net> 