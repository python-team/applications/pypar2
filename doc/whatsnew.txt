[+] New feature
[-] Bug fixed


v1.4 (18/04/2007)
    [+] Added russian translation (Solomin Evgeny)


v1.3 (27/02/07)
    [+] Added dutch translation (Sander Smalbrugge & Rinse de Vries)
    [+] Added a pause button to the output window
    [ ] Added GPL v2 licence text to the archive


v1.2 (08/02/07)
    [+] Added drag & drop support (thanks to Rob Neild)
    [+] When some files are specified on the command line, the listview is automatically populated (thanks to Rob Neild)


v1.1 (30/01/07)
    [+] Added spanish translation (Gumer Coronel Pérez)


v1.0 (20/01/07)
    [+] When a .par2 file is specified on the command line, PyPar2 immediately checks it
    [+] Improved listview usability by splitting strings into directories and filenames
    [+] The entries are now sorted in the listview
    [+] The number of entries in the listview is now displayed on top of it
    [+] Columns in listview are now auto-resized when adding/removing files
    [+] Added a vertical scrollbar to the output window
    [-] Output dialog is no longer destroyed when using the title bar close button
    [-] Dialogs are now correctly resized before being displayed
    [ ] Added some artwork (thanks to Christoph Brill)
    [ ] Preferences format has changed (settings will be reset on first launch)
    [ ] Added some tooltips
    [ ] The output terminal is now cleared before being used
    [ ] Many code changes and improvements


v0.10 (07/11/06)
    [+] Added german translation (Volker Eckert)


v0.09 (13/08/06)
    [+] Applied patch provided by Ludovic Biancotto:
        * Added more spacing to GUI
    [ ] Updated .desktop file
    [ ] Miscellaneous code changes


v0.08 (28/07/06)
    [+] Added multilingual support
        * Added french translation
        * Added polish translation (Piotr Ozarowski)


v0.07 (24/07/06)
    [+] Par2 creation: removed the "Select file(s)" button
    [+] Par2 creation:: added a contextual menu with more actions
    [ ] Added a .desktop file (thanks to Piotr Ozarowski)


v0.06 (19/07/06)
    [ ] Added license in about box
    [ ] Applied patch provided by Piotr Ozarowski:
        * Replace start.sh file with Makefile
        * Removed hashbang from files that should not be executed
        * Fixed consts.py file to avoid changing directory when starting pypar2
        * Added a manpage
        * Fix Free Software Foundation address


v0.05 (18/07/06)
    [+] Added advanced creation settings


v0.04 (14/07/06)
    [+] PyPar2 now handles the creation of par2 files


v0.03 (10/07/06)
    [+] Output dialog is now resizable, and its size is restored across sessions
    [+] Added a button to restore default advanced settings
    [+] Par2 now displays the percentage of progression
    [+] Par2 command may now be interrupted


v0.02 (08/07/06)
    [+] Maximum memory usage may now be specified
    [+] State of widgets is now restored across sessions


v0.01 (06/07/06)
    [ ] First release
