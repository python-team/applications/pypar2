# -*- coding: utf-8 -*-
#
# Author: Ingelrest François (Athropos@gmail.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import consts, gtk, os

#==========================================================
#
# Functions
#
#==========================================================

def createDlg(parent) :
    """
        Create and configure the dialog box
    """
    global dlg, wTree

    wTree = gtk.glade.XML(os.path.join(consts.dirRes, 'dlg-save.glade'), domain=consts.appNameShort)
    dlg   = wTree.get_widget('dlg-save')
    # Configure the dialog
    dlg.set_modal(True)
    dlg.set_transient_for(parent)
    dlg.connect('response', lambda dialog, response : dlg.hide())


def show(parent, defaultFile) :
    """
        Show the dialog box, use defaultFile as the default filename
    """
    if dlg is None :
        createDlg(parent)
    # set_filename() changes the current directory, if the file does not exist then nothing is selected
    # set_current_name() must be used to provide a non-existing filename
    dlg.set_filename(defaultFile)
    slashIndex = defaultFile.rfind('/')
    if slashIndex != -1 :
        dlg.set_current_name(defaultFile[slashIndex+1:])
    # Return the chosen filename, if any
    if dlg.run() == gtk.RESPONSE_OK :
        return dlg.get_filename()
    return None

#==========================================================
#
# Entry point
#
#==========================================================

dlg = None
