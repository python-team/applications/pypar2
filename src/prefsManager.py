# -*- coding: utf-8 -*-
#
# Author: Ingelrest François (Athropos@gmail.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import consts, os, typeToolBox, xml.dom.ext, xml.dom.minidom

from gettext import gettext as _

# Pages in the main notebook
(
    PAGE_CHECK,
    PAGE_CREATE
) = range(2)

#==========================================================
#
# Functions
#
#==========================================================

def resetAdvSettings() :
    """
        Restore the default values of all advanced settings
    """
    # Default values are taken from the man page of par2
    prefs['chkUniformFileSize'] = False
    prefs['spnMemoryUsage']     = 16
    prefs['spnBlockCount']      = 1
    prefs['spnBlockSize']       = 1
    prefs['spnRedundancyLvl']   = 5
    prefs['spnRedundancyCount'] = 100
    prefs['spnRecoveryCount']   = 7
    prefs['spnFirstBlock']      = 0
    prefs['grpBlocks']          = 'radBlockDynamic'
    prefs['grpRedundancy']      = 'radRedundancyLvl'
    prefs['grpParity']          = 'radParityCount'


def save() :
    """
        Save all preferences to the disk
    """
    # Create the XML document
    doc  = xml.dom.minidom.Document()
    root = doc.createElement('prefs')
    root.setAttribute('version', str(consts.prefsVersion))
    doc.appendChild(root)
    # Write each preference to the document
    for prefName in prefs.keys() :
        node      = doc.createElement('pref')
        prefValue = prefs[prefName]
        node.setAttribute('name', prefName)
        node.setAttribute('value', str(prefValue))
        node.setAttribute('type', typeToolBox.getType(prefValue))
        root.appendChild(node)
    # Write the document to the disk
    file_object = open(consts.filePrefs, 'w')
    xml.dom.ext.PrettyPrint(doc, file_object)
    file_object.close()


def set(prefName, prefValue) :
    """
        Change the value of a given preference
    """
    if not prefs.has_key(prefName) :
        raise NameError, prefName + _(' is an unknown preference')
    prefs[prefName] = prefValue;


def get(prefName) :
    """
        Retrieve the value of a given preference
    """
    if not prefs.has_key(prefName) :
        raise NameError, prefName + _(' is an unknown preference')
    return prefs[prefName]

#==========================================================
#
# Entry point
#
#==========================================================

# Basic preferences
prefs = {'outputDlgHeight'   : 400,
         'outputDlgWidth'    : 500,
         'nbkMain'           : PAGE_CHECK,
         'nbkAdvanced'       : 0,
         'grpAction'         : 'radActionRepair',
         'chkUseAdvSettings' : False}
# Advanced preferences
resetAdvSettings()

# Load saved preferences from the disk
if os.access(consts.filePrefs, os.F_OK) :
    root = xml.dom.minidom.parse(consts.filePrefs).documentElement
    # Don't parse a too old prefs file
    if root.hasAttribute('version') and int(root.getAttribute('version')) == consts.prefsVersion :
        for elt in root.getElementsByTagName('pref') :
            name        = elt.getAttribute('name')
            value       = typeToolBox.cast(elt.getAttribute('value'), elt.getAttribute('type'))
            set(name, value)
