# -*- coding: utf-8 -*-
#
# Author: Ingelrest François (Athropos@gmail.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import consts, gtk, os

from gettext import gettext as _

# The modes supported by this dialog box
(
    MODE_FILES,
    MODE_DIRECTORY
) = range(2)

#==========================================================
#
# Functions
#
#==========================================================

def createDlg(parent) :
    """
        Create and configure the dialog box
    """
    global dlg, wTree

    wTree = gtk.glade.XML(os.path.join(consts.dirRes, 'dlg-choosefiles.glade'), domain=consts.appNameShort)
    dlg   = wTree.get_widget('dlg-choosefiles')
    # Configure the dialog
    dlg.set_modal(True)
    dlg.set_transient_for(parent)
    dlg.set_current_folder(consts.dirUsr)
    dlg.connect('response', lambda dialog, response : dlg.hide())


def show(parent, mode) :
    """
        Show the dialog box, using the given mode
    """
    if dlg is None :
        createDlg(parent)
    # Configure the dialog according to mode
    if mode == MODE_FILES :
        dlg.set_action(gtk.FILE_CHOOSER_ACTION_OPEN)
        dlg.set_title(_('Select file(s)'))
        dlg.set_select_multiple(True)
    else :
        dlg.set_action(gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER)
        dlg.set_title(_('Select directory'))
        dlg.set_select_multiple(False)
    # Return the files selected by the user
    if dlg.run() == gtk.RESPONSE_OK :
        return dlg.get_filenames()
    return None

#==========================================================
#
# Entry point
#
#==========================================================

dlg = None
