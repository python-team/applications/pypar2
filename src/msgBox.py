# -*- coding: utf-8 -*-
#
# Author: Ingelrest François (Athropos@gmail.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import gtk

def show(parent, msgType, buttons, msg) :
    """
        Show a message box, using all the given parameters
    """
    dlg = gtk.MessageDialog(parent, gtk.DIALOG_MODAL, msgType, buttons, msg)
    dlg.set_markup(msg)
    response = dlg.run()
    dlg.destroy()
    return response


def error(parent, msg) :
    """
        Show an error message box
    """
    show(parent, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, msg)


def question(parent, msg) :
    """
        Show a question message box and return the selected button
    """
    return show(parent, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, msg)


def information(parent, msg) :
    """
        Show an information message box
    """
    show(parent, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, msg)
