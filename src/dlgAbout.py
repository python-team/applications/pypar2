# -*- coding: utf-8 -*-
#
# Author: Ingelrest François (Athropos@gmail.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import consts, gtk, os.path

from gettext import gettext as _

def show(parent) :
    """
        Show an about dialog box
    """
    dlg = gtk.AboutDialog()
    dlg.set_transient_for(parent)

    # Set credit information
    dlg.set_name(consts.appName)
    dlg.set_version(consts.appVersion)
    dlg.set_website(consts.appURL)
    dlg.set_translator_credits(_('translator-credits'))
    dlg.set_comments(_('A graphical frontend for the par2 utility.'))
    dlg.set_artists(['Christoph Brill <egore911@egore911.de>'])

    dlg.set_authors([
        _('Developer:'),
        'Ingelrest François <Athropos@gmail.com>',
        '',
        _('Thanks to:'),
        ' * Piotr Ozarowski',
        ' * Ludovic Biancotto',
        ' * Rob Neild',
    ])

    # Set logo
    if os.path.exists(consts.fileImgAbout) :
        dlg.set_logo(gtk.gdk.pixbuf_new_from_file(consts.fileImgAbout))

    # Load the licence from the disk if possible
    if os.path.exists(consts.fileLicense) :
        dlg.set_license(open(consts.fileLicense).read())
        dlg.set_wrap_license(True)

    dlg.run()
    dlg.destroy()
